// Kiểu dữ liệu trong JS

let a = '1';
var b = 2;

//! var - let - const - loop: {}
// ví dụ Let và Var
let isMinhNgu = false;

isMinhNgu = true;
console.log( isMinhNgu );
if (isMinhNgu) {
    var minhSua = 'gâu gâu';
    let bauSua = 'gâu gâu';
}

console.log('Minh sủa:', minhSua);
// const
const pi = 3.14;
let newPi = pi * 2;
console.log(newPi);

// so sánh - toán tử

if (a !== b) {
    console.log('tốt');
}
a !== b ? console.log('Báu') : console.log('Minh') ;

//! Vòng lặp
//? vòng lặp for
let index = 0
for ( ; index < array.length ; ) {
    const element = array[index];
    index++
}

// ? for...of
var Numbers = [1,2,3,4,5,6,7,8,9]
for (let num of Numbers) {
    console.log(`${num}-------${Numbers[num]}`);
}

// length of array
console.log(Numbers.length);

// ? forEach
Numbers.forEach(element => {
    console.log(element);
});

// ? for..in
for (const key in object) {
    if (object.hasOwnProperty(key)) {
        const element = object[key];
        
    }
}

var Minh = {
    bark: function () {
        console.log('Gâu gâu');
    },
    name: 'Lê Minh',
    sex: 'gay',
    action: ['bark', 'sleep', 'eat', 'walk'],
}

console.log(Minh['sex']);
console.log(Minh.sex);

for (const keyword in Minh) {
    console.log(Minh[keyword]);
}

Minh.bark()

//! callback function
function showMeYourGay() {
    console.log('Minh is Gay');
}

function whoILoveMost() {
    console.log('Anh Sơn');
}

function countAndPlay(callback) {
    for (let i = 0; i < 10; i++) {
        console.log(i + 1);
    }

    callback();
}

countAndPlay(showMeYourGay);
countAndPlay(whoILoveMost);